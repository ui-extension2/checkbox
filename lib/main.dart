import 'package:flutter/material.dart';
import 'package:ui_extension/combobox_widget.dart';

void main() {
  runApp(MaterialApp(
      title: 'Ui Extension', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ui Extension')),
        body: ListView(children: [
          ListTile(
            title: Text('ComboBox'),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => ComboBoxWidget()));
            },
          )
        ],),
      );
  }
}